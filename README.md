Overview: 
-An app that gets a list of items from an API endpoint and displays them in a list page.

Specs (descending order of priority):

1) At app start, and everytime the app resumes, the app will issue a GET request from https://jsonplaceholder.typicode.com/posts

2) The format of the result is a list of JSON objects with the following fields: "userId","id","title","body"

3) The app will then display the results in a list. Every list item must show the title of the object, and the body of the object.

4) Clicking a list item will show a toast containing the id and the userId.

5) At the top of the list there is a searchbar (or edit text) and a button. Upon clicking the button, the app will issue a GET request on the same endpoint (https://jsonplaceholder.typicode.com/posts) with the following extra parameter "userId=<THE TEXT INSIDE THE SEARCHBAR>" and will replace the contents of the list after a successful request.

6) Server requests must be made in a separate thread, not the main thread. You can use AsyncTask, Service, Thread, etc. whichever you prefer.

7) The UX and the UI is up to you.

8) Email me the zip file of your project or a github project by tomorrow 12PM, whichever you prefer.

Notes:
-Android has a built-in JSON parser, but it is heavily recommended to use the Google GSON library
-Do whatever you can. It is heavily recommended to do everything.
-Open notes open everything
-If you have any questions about the specs, message/email me.
-If you have any questions about implementation, there should be an answered stackoverflow question for that, if still confused then you can message me.

