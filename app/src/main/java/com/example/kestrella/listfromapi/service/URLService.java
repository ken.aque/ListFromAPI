package com.example.kestrella.listfromapi.service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.kestrella.listfromapi.R;
import com.example.kestrella.listfromapi.data.PostDTO;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by kestrella on 9/15/16.
 */
public class URLService extends AsyncTask<String, Void, String> {
    private Context con;
    private PostDTO[] postDTOs;
    private View v;
    ArrayList<Integer> ids = new ArrayList<>();
    ArrayList<Integer> userIds = new ArrayList<>();
    ArrayList<String> titles = new ArrayList<>();
    ArrayList<String> bodies = new ArrayList<>();

    public URLService(Context context, View view) {
        this.con = context;
        this.v = view;

    }

    @Override
    protected String doInBackground(String... urls) {
        try {
            makeRequest(urls[0]);
        } catch(Exception e) {
            return e.toString();
        }
        return null;
    }

    private void makeRequest(String url) {
        RequestQueue queue = Volley.newRequestQueue(this.con);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try{
                    postDTOs = convertToObject(response);
                    String len = String.valueOf(postDTOs.length);
                    String message = len + " results found.";
                    Toast.makeText(con, message, Toast.LENGTH_SHORT).show();
                    makeSimpleList();
                } catch (Exception e) {
                    Log.d("Debug", e.toString());
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Debug", error.toString());
            }
        });

        try{
            queue.add(strReq);
        } catch (Exception e){
            Log.d("Debug", e.toString());
        }
    }

    // make sure to map the json string into structured object
    private PostDTO[] convertToObject(String jsonString) {
        Gson gson = new Gson();
        JsonReader reader = new JsonReader(new StringReader(jsonString));
        reader.setLenient(true);
        return gson.fromJson(reader, PostDTO[].class);
    }

    private void makeSimpleList() {
        for(PostDTO postDTO: postDTOs) {
            ids.add(postDTO.getId());
            userIds.add(postDTO.getUserId());
            titles.add(postDTO.getTitle());
            bodies.add(postDTO.getBody());
        }
        List<HashMap<String, String>> resultsList = new ArrayList<>();
        resultsList.clear();
        render(resultsList);
    }

    private void render(final List<HashMap<String, String>> resultsList) {
        for (int i = 0; i < ids.size(); i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("title", titles.get(i));
            hm.put("body", bodies.get(i));
            resultsList.add(hm);
        }

        String[] from = {"title", "body"};
        final int[] to = {R.id.txtTitle, R.id.txtBody};
        SimpleAdapter adapter = new SimpleAdapter(con, resultsList,
                R.layout.itemlayout, from, to);

        final ListView listView = (ListView) v;
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String output = "ID: " + ids.get(position) + "\n" +
                                 "User ID: " + userIds.get(position);
                Toast.makeText(con, output , Toast.LENGTH_SHORT).show();
            }
        });

    }

}
