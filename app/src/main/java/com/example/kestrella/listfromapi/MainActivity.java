package com.example.kestrella.listfromapi;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.SearchView;
import android.widget.Toast;

import com.example.kestrella.listfromapi.service.URLService;

public class MainActivity extends AppCompatActivity {
    final String rootURL = "https://jsonplaceholder.typicode.com/posts";
    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        searchView = (SearchView)findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                if(!query.isEmpty()) {
                    String newURL = rootURL + "?userId=" + query;
                    connect(newURL);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        connect(rootURL);


    }

    private void connect(String url) {
        if(isConnectionOK()) {
            new URLService(getApplicationContext(), findViewById(R.id.listView)).execute(url);
        } else {
            Toast.makeText(this, "Make sure you are connected to a network.", Toast.LENGTH_LONG).show();
        }
    }

    private boolean isConnectionOK() {
        try {
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            return networkInfo != null && networkInfo.isConnected();
        } catch(Exception e) {
            Log.d("Debug", e.toString());
            return false;
        }

    }
}
